
require("dotenv").config()
const User = require("../models/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken")

module.exports = {

 /**************************GET_USERS_LIST*************************************/ 
  async getusersList(req, res) {
    try {
      const data = await User.find({});
      res.json(data);
    } catch (err) {
      console.error(err);
      res.status(500).send("Server error");
    }
  },
/**************************ADD USERS*************************************/
  async addUser(req, res) {
    try {
      const user = await User.findOne({email:req.body.email})
      if(!user){
        let hashedpassword = await bcrypt.hash(req.body.password, 10);
        const newUser = new User({
          email:req.body.email,
          password:hashedpassword
        });
        await newUser.save();
        res.json({
          status:200,
          success:true,
          message:"user has been created successfully",
          user:newUser
        });
      }else{
        res.json({
          success:false,
          message:"user already exist",
        })
      }
    } catch (err) {
      console.log(err);
      res.json({
        status:500,
        success:false,
        message:err,
      })
    }
  },
  /**************************USER LOGIN*************************************/
  async userlogin(req,res){
    try {
      let responseUserData ={}
      const { email, password } = req.body;
      const user = await User.findOne({ email });
      if (!user) {
        return res.status(401).json({ error: 'Invalid email or password' });
      }
      const passwordMatch = await bcrypt.compare(password, user.password);
      if (!passwordMatch) {
        return res.status(401).json({ error: 'Invalid email or password' });
      }
      const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET);
      user.token=token
      responseUserData.email=user.email
      responseUserData.id=user._id
      responseUserData.token=token
      res.json(responseUserData)
     

    } catch (err) {
      res.status(500).json({ error: err.message });
    }

  },
/**************************USER_DETAIL*************************************/
  async singleUserDetail(req, res) {
    try {
      const data = await User.findById(req.params.id);
      if (!data) {
        return res.status(404).json({ error: "Data not found" });
      }
      res.json(data);
    } catch (err) {
      console.error(err);
      res.status(500).send("server error");
    }
  },
/**************************DELETE_USER*************************************/
  async deleteUser(req, res) {
    try {
      const data = await User.findByIdAndRemove(req.params.id);
      if (!data) {
        return res.status(404).json({ error: "User not found" });
      }
      const users = await User.find();
      res.json(users);
    } catch (err) {
      console.error(err);
      res.status(500).send("server error");
    }
  },
};
