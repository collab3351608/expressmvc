const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const isAuth =require("../middlewares/auth")
/*********************User Handling*******************************/
router.get("/users",isAuth, userController.getusersList);
router.post("/signup", userController.addUser);
router.get("/user/:id",isAuth, userController.singleUserDetail); 
router.delete("/user/:id", userController.deleteUser);
router.post("/login", userController.userlogin);
/*******************************************************************/
module.exports = router;
