require("dotenv").config()
const express = require("express");
const port = 8000;
const app = express();
const passport = require('passport');
const userRouter = require("./routes/user");
const cors = require("cors");
const db = require("./db");0
const StrategyJWT = require('./config/passport')
StrategyJWT(passport)
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());

app.use("/", userRouter);

app.listen(port, () => {
  console.log(`Server is Up on Port :${port}`);
});
